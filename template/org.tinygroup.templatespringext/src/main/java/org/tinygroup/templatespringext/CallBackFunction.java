package org.tinygroup.templatespringext;

import org.tinygroup.vfs.FileObject;

/**
 * Created by wangll13383 on 2015/9/12.
 */
public interface CallBackFunction {
    public void process(FileObject fileObject);
}
